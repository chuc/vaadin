package com.ex.chucnd.data.entity;

import com.ex.chucnd.data.AbstractEntity;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class User extends AbstractEntity {

    private String userName;
    private Integer status;
    private LocalDate userStartTime;
    private LocalDate userEndTime;
    private LocalDate createdDate;
    private String menuId;
    private String phoneNumber;
    private String password;
    private String description;
    private String role;
    private String fullName;
    private String address;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDate getUserStartTime() {
        return userStartTime;
    }

    public void setUserStartTime(LocalDate userStartTime) {
        this.userStartTime = userStartTime;
    }

    public LocalDate getUserEndTime() {
        return userEndTime;
    }

    public void setUserEndTime(LocalDate userEndTime) {
        this.userEndTime = userEndTime;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
