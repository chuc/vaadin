package com.ex.chucnd.data.service;

import com.ex.chucnd.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<User, Integer> {

}