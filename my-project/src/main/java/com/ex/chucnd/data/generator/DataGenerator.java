package com.ex.chucnd.data.generator;

import com.ex.chucnd.data.entity.User;
import com.vaadin.flow.spring.annotation.SpringComponent;

import com.ex.chucnd.data.service.PersonRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.vaadin.artur.exampledata.DataType;
import org.vaadin.artur.exampledata.ExampleDataGenerator;

@SpringComponent
public class DataGenerator {

    @Bean
    public CommandLineRunner loadData(PersonRepository personRepository) {
        return args -> {
            Logger logger = LoggerFactory.getLogger(getClass());
            if (personRepository.count() != 0L) {
                logger.info("Database đã sử dụng!");
                return;
            }
            int seed = 123;

            logger.info("... Tạo dữ liệu 100 người dùng ... !");
            ExampleDataGenerator<User> personRepositoryGenerator = new ExampleDataGenerator<>(User.class);
            personRepositoryGenerator.setData(User::setId, DataType.ID);
            personRepositoryGenerator.setData(User::setFullName, DataType.FIRST_NAME);
            personRepositoryGenerator.setData(User::setUserName, DataType.LAST_NAME);
            personRepositoryGenerator.setData(User::setPassword, DataType.EMAIL);
            personRepositoryGenerator.setData(User::setPhoneNumber, DataType.PHONE_NUMBER);
            personRepositoryGenerator.setData(User::setRole, DataType.FULL_NAME);
            personRepositoryGenerator.setData(User::setStatus, DataType.NUMBER_UP_TO_10);
            personRepositoryGenerator.setData(User::setDescription, DataType.WORD);
            personRepository.saveAll(personRepositoryGenerator.create(100, seed));

            logger.info("Tạo dữ liệu test!");
        };
    }
}