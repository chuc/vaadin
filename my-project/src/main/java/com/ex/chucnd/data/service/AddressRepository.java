package com.ex.chucnd.data.service;

import com.ex.chucnd.data.entity.Address;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {

}