package com.ex.chucnd.data.service;

import com.ex.chucnd.data.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

@Service
public class ItemService extends CrudService<Item, Integer> {

    private ItemRepository repository;

    public ItemService(@Autowired ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    protected ItemRepository getRepository() {
        return repository;
    }

}
