package com.ex.chucnd.data.entity;

import com.ex.chucnd.data.AbstractEntity;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
public class Item extends AbstractEntity {

    private String itemCd;
    private String itemBarcode;
    private long itemPrice;
    private String itemInputPrice;
    private long itemQuantity;
    private int itemGroup;

    public String getItemCd() {
        return itemCd;
    }
    public void setItemCd(String itemCd) {
        this.itemCd = itemCd;
    }
    public String getItemBarcode() {
        return itemBarcode;
    }
    public void setItemBarcode(String itemBarcode) {
        this.itemBarcode = itemBarcode;
    }
    public Long getItemPrice() {
        return itemPrice;
    }
    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }
    public String getItemInputPrice() {
        return itemInputPrice;
    }
    public void setItemInputPrice(String itemInputPrice) {
        this.itemInputPrice = itemInputPrice;
    }
    public int getItemGroup() {
        return itemGroup;
    }
    public void setItemGroup(int itemGroup) {
        this.itemGroup = itemGroup;
    }
    public long getItemQuantity() {
        return itemQuantity;
    }
    public void setItemQuantity(long itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

}
