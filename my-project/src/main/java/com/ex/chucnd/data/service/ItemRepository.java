package com.ex.chucnd.data.service;

import com.ex.chucnd.data.entity.Item;
import com.ex.chucnd.data.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}