package com.ex.chucnd.views.list;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.ex.chucnd.data.entity.Item;
import com.ex.chucnd.data.service.ItemService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.Binder;
import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.gridpro.GridPro;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.ex.chucnd.views.main.MainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Route(value = "list", layout = MainView.class)
@PageTitle("Danh sách mặt hàng")
@CssImport(value = "./styles/views/list/list-view.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
public class ListView extends Div {

    private GridPro<Item> grid;
    private ListDataProvider<Item> dataProvider;

    @Autowired
    private ItemService itemService;

    private Grid.Column<Item> idColumn;
    private Grid.Column<Item> itemCodeColumn;
    private Grid.Column<Item> barcodeColumn;
    private Grid.Column<Item> priceColumn;
    private Grid.Column<Item> quantityColumn;
    private Grid.Column<Item> groupColumn;

    private TextField itemCd = new TextField("Mã sản phẩm");
    private TextField itemBarcode = new TextField("BARCODE");
    private TextField itemPrice = new TextField("Giá sản phẩm");
    private TextField itemQuantity = new TextField("Số lượng");
    private TextArea itemGroup = new TextArea("Loại sản phẩm");

    private Button cancel = new Button("Hủy");
    private Button save = new Button("Lưu");

    private  Item itemModel = new Item();

    private Binder<Item> binder = new Binder<>(Item.class);

    public ListView() {
        setId("list-view");
        setSizeFull();
        createGrid();
        add(createFormLayout());
        add(createButtonLayout());
        add(grid);
    }

    private void createGrid() {
        createGridComponent();
        addColumnsToGrid();
        addFiltersToGrid();
    }

    private void createGridComponent() {
        grid = new GridPro<>();
        grid.setSelectionMode(SelectionMode.MULTI);
        grid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_COLUMN_BORDERS);
        grid.setPageSize(1);
        grid.setHeight("60%");

        dataProvider = new ListDataProvider<>(getItem());

        grid.setDataProvider(dataProvider);
        cancel.addClickListener(e -> clearForm());
        save.addClickListener(e -> {
            if(e != null) {
                try {
                    loadData(itemCd.getValue(), itemBarcode.getValue(), Long.parseLong(itemPrice.getValue()),
                            Long.parseLong(itemQuantity.getValue()), Integer.parseInt(itemGroup.getValue()));
                    itemService.update(binder.getBean());
                } catch (Exception ex) {
                    Notification.show("Thêm mới thất bại.");
                    return;
                }
                Notification.show("Thêm mới thành công.");
                clearForm();
            }
        });
    }

    private void addColumnsToGrid() {
        createIdColumn();
        createItemCdColumn();
        createBarocdeColumn();
        createPriceColumn();
        createQuantityColumn();
//        createDateColumn();
    }

    private void createIdColumn() {
        idColumn = grid.addColumn(Item::getId, "id").setHeader("ID").setWidth("120px").setFlexGrow(0);
    }

    /**
     * Tạo cột mã sản phẩm
     */
    private void createItemCdColumn() {
        itemCodeColumn = grid.addColumn(new ComponentRenderer<>(item -> {
            HorizontalLayout hl = new HorizontalLayout();
            hl.setAlignItems(Alignment.CENTER);
            Span span = new Span();
            span.setClassName("name");
            span.setText(item.getItemCd());
            return hl;
        })).setComparator(item -> item.getItemCd()).setHeader("Mã sản phẩm");
    }

    /**
     * Tạo cột barcode
     */
    private void createBarocdeColumn() {
        barcodeColumn = grid.addColumn(new ComponentRenderer<>(client -> {
            HorizontalLayout hl = new HorizontalLayout();
            hl.setAlignItems(Alignment.CENTER);
            Span span = new Span();
            span.setClassName("name");
            span.setText(client.getItemBarcode());
            return hl;
        })).setComparator(client -> client.getItemBarcode()).setHeader("Mã barcode");
    }

    /**
     * Tạo cột giá
     */
    private void createPriceColumn() {
        priceColumn = grid.addColumn(new ComponentRenderer<>(item -> {
            HorizontalLayout hl = new HorizontalLayout();
            hl.setAlignItems(Alignment.CENTER);
            Span span = new Span();
            span.setClassName("name");
            span.setText(String.valueOf(item.getItemPrice()));
            return hl;
        })).setComparator(item -> item.getItemPrice()).setHeader("Giá sản phẩm");
    }

    /**
     * Tạo cột số lượng
     */
    private void createQuantityColumn() {
        quantityColumn = grid.addColumn(new ComponentRenderer<>(item -> {
            HorizontalLayout hl = new HorizontalLayout();
            hl.setAlignItems(Alignment.CENTER);
            Span span = new Span();
            span.setClassName("name");
            span.setText(String.valueOf(item.getItemQuantity()));
            return hl;
        })).setComparator(item -> item.getItemQuantity()).setHeader("Số lượng sản phẩm");
    }

//    private void createDateColumn() {
//        dateColumn = grid
//                .addColumn(new LocalDateRenderer<>(client -> LocalDate.parse(client.getDate()),
//                        DateTimeFormatter.ofPattern("M/d/yyyy")))
//                .setComparator(client -> client.getDate()).setHeader("Date").setWidth("180px").setFlexGrow(0);
//    }


    private void addFiltersToGrid() {
        HeaderRow filterRow = grid.appendHeaderRow();

        TextField idFilter = new TextField();
        idFilter.setPlaceholder("Filter");
        idFilter.setClearButtonVisible(true);
        idFilter.setWidth("100%");
        idFilter.setValueChangeMode(ValueChangeMode.EAGER);
        idFilter.addValueChangeListener(event -> dataProvider.addFilter(
                client -> StringUtils.containsIgnoreCase(Integer.toString(client.getId()), idFilter.getValue())));
        filterRow.getCell(idColumn).setComponent(idFilter);

        TextField clientFilter = new TextField();
        clientFilter.setPlaceholder("Filter");
        clientFilter.setClearButtonVisible(true);
        clientFilter.setWidth("100%");
        clientFilter.setValueChangeMode(ValueChangeMode.EAGER);
        clientFilter.addValueChangeListener(event -> dataProvider
                .addFilter(client -> StringUtils.containsIgnoreCase(client.getItemCd(), clientFilter.getValue())));
        filterRow.getCell(itemCodeColumn).setComponent(clientFilter);

        TextField amountFilter = new TextField();
        amountFilter.setPlaceholder("Filter");
        amountFilter.setClearButtonVisible(true);
        amountFilter.setWidth("100%");
        amountFilter.setValueChangeMode(ValueChangeMode.EAGER);
        amountFilter.addValueChangeListener(event -> dataProvider.addFilter(client -> StringUtils
                .containsIgnoreCase(client.getItemBarcode(), amountFilter.getValue())));
        filterRow.getCell(barcodeColumn).setComponent(amountFilter);

//        ComboBox<String> statusFilter = new ComboBox<>();
//        statusFilter.setItems(Arrays.asList("Pending", "Success", "Error"));
//        statusFilter.setPlaceholder("Filter");
//        statusFilter.setClearButtonVisible(true);
//        statusFilter.setWidth("100%");
//        statusFilter.addValueChangeListener(
//                event -> dataProvider.addFilter(client -> areStatusesEqual(client, statusFilter)));
//        filterRow.getCell(priceColumn).setComponent(statusFilter);

        TextField priceFilter = new TextField();
        priceFilter.setPlaceholder("Filter");
        priceFilter.setClearButtonVisible(true);
        priceFilter.setWidth("100%");
        priceFilter.setValueChangeMode(ValueChangeMode.EAGER);
        priceFilter.addValueChangeListener(event -> dataProvider.addFilter(item -> StringUtils
                .containsIgnoreCase(String.valueOf(item.getItemPrice()), priceFilter.getValue())));
        filterRow.getCell(priceColumn).setComponent(priceFilter);


        TextField quantityFilter = new TextField();
        quantityFilter.setPlaceholder("Filter");
        quantityFilter.setClearButtonVisible(true);
        quantityFilter.setWidth("100%");
        quantityFilter.setValueChangeMode(ValueChangeMode.EAGER);
        quantityFilter.addValueChangeListener(event -> dataProvider.addFilter(item -> StringUtils
                .containsIgnoreCase(String.valueOf(item.getItemQuantity()), quantityFilter.getValue())));
        filterRow.getCell(quantityColumn).setComponent(quantityFilter);

//        DatePicker dateFilter = new DatePicker();
//        dateFilter.setPlaceholder("Filter");
//        dateFilter.setClearButtonVisible(true);
//        dateFilter.setWidth("100%");
//        dateFilter.addValueChangeListener(event -> dataProvider.addFilter(client -> areDatesEqual(client, dateFilter)));
//        filterRow.getCell(dateColumn).setComponent(dateFilter);
    }

    private boolean areStatusesEqual(Client client, ComboBox<String> statusFilter) {
        String statusFilterValue = statusFilter.getValue();
        if (statusFilterValue != null) {
            return StringUtils.equals(client.getStatus(), statusFilterValue);
        }
        return true;
    }

    private boolean areDatesEqual(Client client, DatePicker dateFilter) {
        LocalDate dateFilterValue = dateFilter.getValue();
        if (dateFilterValue != null) {
            LocalDate clientDate = LocalDate.parse(client.getDate());
            return dateFilterValue.equals(clientDate);
        }
        return true;
    }

    /**
     * Tạo form thêm mới sản phẩm
     * @return
     */
    private Component createFormLayout() {
        FormLayout formLayout = new FormLayout();
        itemPrice.setErrorMessage("Giá sản phẩm bắt buộc nhập");
        itemQuantity.setErrorMessage("Số lượng sản phẩm bắt buộc nhập");
        HorizontalLayout row1 = new HorizontalLayout();
        row1.add(itemCd, itemBarcode);
        row1.getStyle().set("border", "0.5px solid white");
        formLayout.addComponentAtIndex(0, row1);

        row1 = new HorizontalLayout();
        row1.add(itemPrice, itemQuantity);
        row1.getStyle().set("border", "0.5px solid white");
        formLayout.addComponentAtIndex(1, row1);

        row1 = new HorizontalLayout();
        row1.add(itemGroup);
        row1.getStyle().set("border", "0.5px solid white");
        formLayout.addComponentAtIndex(2, row1);
        return formLayout;
    }

    /**
     * Tạo button
     * @return
     */
    private Component createButtonLayout() {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.addClassName("button-layout");
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonLayout.add(save);
        buttonLayout.add(cancel);
        return buttonLayout;
    }

    private void clearForm() {
        itemCd.setValue("");
        itemBarcode.setValue("");
        itemPrice.setValue("");
        itemQuantity.setValue("");
        itemGroup.setValue("");
        binder.setBean(new Item());
    }

    private void loadData(String itemCd, String itemBarcode, long itemPrice, long itemQuantity, int itemGroup) {
        Item model = new Item();
        this.itemModel.setItemCd(itemCd);
        this.itemModel.setItemBarcode(itemBarcode);
        this.itemModel.setItemPrice(itemPrice);
        this.itemModel.setItemQuantity(itemQuantity);
        this.itemModel.setItemGroup(itemGroup);
        binder.setBean(model);
    }

    private List<Item> getItem() {
//        return itemService.list();
        return null;
    }
}
