package com.ex.chucnd.views.dashboard;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.board.Board;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.ChartType;
import com.vaadin.flow.component.charts.model.Configuration;
import com.vaadin.flow.component.charts.model.Crosshair;
import com.vaadin.flow.component.charts.model.ListSeries;
import com.vaadin.flow.component.charts.model.XAxis;
import com.vaadin.flow.component.charts.model.YAxis;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.ex.chucnd.views.main.MainView;
import com.vaadin.flow.router.RouteAlias;

@Route(value = "dashboard", layout = MainView.class)
@PageTitle("Trang chủ")
@CssImport(value = "./styles/views/dashboard/dashboard-view.css", include = "lumo-badge")
@JsModule("@vaadin/vaadin-lumo-styles/badge.js")
@RouteAlias(value = "", layout = MainView.class)
public class DashboardView extends Div implements AfterNavigationObserver {

    private Grid<HealthGridItem> grid = new Grid<>();

    private Chart monthlyVisitors = new Chart();
    private Chart responseTimes = new Chart();
    private final H2 usersH2 = new H2();
    private final H2 eventsH2 = new H2();
    private final H2 conversionH2 = new H2();

    public DashboardView() {
        setId("dashboard-view");
        Board board = new Board();
        board.addRow(createBadge("Số lượng", usersH2, "primary-text", "Số lượng mặt hàng đã bán trong ngày", "badge"),
                createBadge("Doanh thu", eventsH2, "success-text", "Số lượng doanh thu trong ngày", "badge success"));

        monthlyVisitors.getConfiguration().setTitle("Doanh thu trong năm");
        monthlyVisitors.getConfiguration().getChart().setType(ChartType.COLUMN);
        WrapperCard monthlyVisitorsWrapper = new WrapperCard("wrapper", new Component[]{monthlyVisitors}, "card");
        board.add(monthlyVisitorsWrapper);

        grid.addColumn(HealthGridItem::getCity).setHeader("City");
        grid.addColumn(new ComponentRenderer<>(item -> {
            Span span = new Span(item.getStatus());
            span.getElement().getThemeList().add(item.getTheme());
            return span;
        })).setFlexGrow(0).setWidth("100px").setHeader("Status");
        grid.addColumn(HealthGridItem::getItemDate).setHeader("Date").setWidth("140px");
        grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);

//        WrapperCard gridWrapper = new WrapperCard("wrapper", new Component[]{new H3("Service health"), grid}, "card");
//        responseTimes.getConfiguration().setTitle("Response times");
//        WrapperCard responseTimesWrapper = new WrapperCard("wrapper", new Component[]{responseTimes}, "card");
//        board.addRow(gridWrapper, responseTimesWrapper);

        add(board);
    }

    private WrapperCard createBadge(String title, H2 h2, String h2ClassName, String description, String badgeTheme) {
        Span titleSpan = new Span(title);
        titleSpan.getElement().setAttribute("theme", badgeTheme);

        h2.addClassName(h2ClassName);

        Span descriptionSpan = new Span(description);
        descriptionSpan.addClassName("secondary-text");

        return new WrapperCard("wrapper", new Component[]{titleSpan, h2, descriptionSpan}, "card", "space-m");
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {

        // Thống kê doanh số
        usersH2.setText("745");
        eventsH2.setText("54.6k");

        //Tạo dữ liệu cho biểu đồ
        Configuration configuration = monthlyVisitors.getConfiguration();
        configuration.addSeries(new ListSeries("", 49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4));

        XAxis x = new XAxis();
        x.setCrosshair(new Crosshair());
        x.setCategories("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        configuration.addxAxis(x);

        YAxis y = new YAxis();
        y.setMin(0);
        configuration.addyAxis(y);

        //  Danh sách thông tin có slide bar
//        List<HealthGridItem> gridItems = new ArrayList<>();
//        String dateChart =  LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

//        gridItems.add(new HealthGridItem(dateChart, "M\u00FCnster", "Germany", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Cluj-Napoca", "Romania", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Ciudad Victoria", "Mexico", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Ebetsu", "Japan", "Excellent", "badge success"));
//        gridItems.add(new HealthGridItem(dateChart, "S\u00E3o Bernardo do Campo", "Brazil", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Maputo", "Mozambique", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Warsaw", "Poland", "Good", "badge"));
//        gridItems.add(new HealthGridItem(dateChart, "Kasugai", "Japan", "Failing", "badge error"));
//        gridItems.add(new HealthGridItem(dateChart, "Lancaster", "United States", "Excellent", "badge success"));

//        grid.setItems(gridItems);

    }
}
